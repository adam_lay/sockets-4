var port = process.env.PORT || 5000;

import path = require("path");
import express = require("express");
var bodyParser = require("body-parser");
import Data = require("./Data");
import Authentication = require("./Authentication");

Data.Init();

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post("/login", (req: express.Request, res) =>
{
  console.log(req.body);
});

app.listen(port);
