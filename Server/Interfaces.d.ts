interface IUser
{
  Id?: number;
  Username: string;
  Password?: string;
  AuthToken?: string;
}

interface IAuthToken
{
  Token: string;
  UserId: number;
  Expiry: string;
}
