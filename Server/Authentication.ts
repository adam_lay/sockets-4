import Data = require("./Data");
var uuid = require("uuid");
var bcrypt = require("bcrypt");

var addHours = function (d, h)
{
  d.setTime(d.getTime() + (h * 60 * 60 * 1000));

  return d;
}

class Authentication
{
  public static TokenTimeout: number = 1; // Hours

  public static Register(username: string, pwd: string, callback: (success: boolean) => void): void
  {
    Data.Get("Users", "Username = '" + username + "'", function (results: Array<IUser>)
    {
      if (results.length > 0)
      {
        callback(false);
        return;
      }

      bcrypt.hash(pwd, 10, function (err, hash)
      {
        Data.Insert("Users", [{ Username: username, Password: hash }], function ()
        {
          callback(true);
        });
      });
    });
  }

  public static Login(username: string, pwd: string, callback: (success: IAuthToken) => void): void
  {
    Data.Get("Users", "Username = '" + username + "'", function (results: Array<IUser>)
    {
      if (results.length < 1)
      {
        callback(null);
        return;
      }

      var usr = results[0];

      bcrypt.compare(pwd, usr.Password, function (err, res)
      {
        if (res)
        {
          Authentication.GetAuthToken(usr.Id, true, function (token: IAuthToken)
          {
            callback(token);
          });
        }
        else
        {
          callback(null);
        }
      });
    });
  }

  public static IsValid(user: IUser, callback: (valid: boolean) => void): void
  {
    Authentication.GetAuthToken(user.Id, false, function (token: IAuthToken)
    {
      callback(token && token.Token == user.AuthToken);
    });
  }

  public static GetAuthToken(userid: number, create: boolean, callback: Function): void
  {
    var expiry = addHours(new Date(), Authentication.TokenTimeout);

    Data.Get("AuthTokens", "UserId = " + userid, function (results)
    {
      if (results.length > 0)
      {
        Data.Update("AuthTokens", { Expiry: expiry }, "", function ()
        {
          callback(results[0]);
        });
      }
      else if (create)
      {
        var newToken: IAuthToken = {
          Token: uuid.v1(),
          UserId: userid,
          Expiry: expiry.toString()
        };

        Data.Insert("AuthTokens", [newToken], function ()
        {
          Data.Get("AuthTokens", "UserId = " + userid, function (newTokens)
          {
            callback(newTokens[0]);
          });
        });
      }
      else
      {
        callback(null);
      }
    });
  }
}

export = Authentication;
