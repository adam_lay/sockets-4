var sqlEscape = function (str)
{
  return (str + "").replace(/'/g, "''");
}

var sqlite3 = require('sqlite3').verbose();

class Data
{
  // Sqlite3 Database
  private static _db;

  public static Init()
  {
    var db = new sqlite3.Database('sockets-4.sqlite3');

    Data._db = db;

    // Create Users Table
    Data.CreateTable("Users", [
      "Id INTEGER PRIMARY KEY AUTOINCREMENT",
      "Username TEXT NOT NULL",
      "Password TEXT NOT NULL"
    ]);

    // Create AuthTokens Table
    Data.CreateTable("AuthTokens", [
      "Token TEXT PRIMARY KEY",
      "UserId INTEGER NOT NULL",
      "Expiry TEXT NOT NULL",
      "FOREIGN KEY(UserId) REFERENCES Users(Id)"
    ]);
  }

  public static CreateTable(name: string, fields: Array<string>): void
  {
    var query = "CREATE TABLE IF NOT EXISTS " + name;

    query += " (" + fields.join(", ") + ");";

    Data._db.run(query);
  }

  public static Get(table: string, query: string, callback: Function)
  {
    Data._db.all("SELECT * FROM " + table + (query ? " WHERE " + query : ""), function (err, rows)
    {
      callback(rows);
    });
  }

  public static GetTop(table: string, query: any, count: number, callback: Function)
  {
    Data.Get(table, query, function (rows)
    {
      callback(rows.slice(0, count));
    });
  }

  // Will work provided the object matches table structure
  public static Insert(table: string, data: Array<Object>, callback: Function)
  {
    if (data.length < 1)
      return;

    var props = "";

    var j = 0;

    for (var p in data[0])
      props += (j++ == 0 ? "" : ",") + p;

    for (var i = 0; i < data.length; i++)
    {
      var vals = "";

      j = 0;

      for (var p in data[i])
      {
        var val = data[i][p];

        vals += j++ == 0 ? "" : ",";

        if (typeof (val) == "object")
          val = JSON.stringify(val);

        if (typeof (val) == "string")
          val = "'" + val + "'";

        vals += val;
      }

      var query = "INSERT INTO " + table + "(" + props + ") VALUES (" + vals + ")";

      Data._db.run(query, callback);
    }
  }

  public static Update(table: string, set: Object, where: string, callback: Function)
  {
    var setStr = "";

    var i = 0;

    for (var prop in set)
    {
      setStr += i++ == 0 ? "" : ",";

      var val = set[prop];

      setStr += prop + " = " + (typeof (val) == "string" ? "'" + sqlEscape(val) + "'" : val);
    }

    var query = "UPDATE " + table + " SET " + setStr + " WHERE " + where

    Data._db.run(query, callback);
  }

  public static Delete(table: string, where: string, callback: Function)
  {
    Data._db.run("DELETE FROM " + table + " WHERE " + where, callback);
  }

  public static Custom(query: (db: any) => void)
  {
    query(Data._db);
  }
}

export = Data;
